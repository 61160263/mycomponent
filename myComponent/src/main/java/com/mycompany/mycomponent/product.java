/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author pet00
 */
public class product {
    private int id;
    private String name;
    private double price;
    private String image;
    
    public product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<product> genProductList(){
        ArrayList<product> list = new ArrayList<>();
        list.add(new product(1, "coffee1", 40, "1.jpg"));
        list.add(new product(1, "coffee2", 30, "2.jpg"));
        list.add(new product(1, "coffee3", 40, "3.jpg"));
        list.add(new product(1, "tea1", 50, "4.jpg"));
        list.add(new product(1, "tea2", 40, "5.jpg"));
        list.add(new product(1, "tea3", 60, "6.jpg"));
        list.add(new product(1, "coffee5", 30, "7.jpg"));
        list.add(new product(1, "coffee6", 70, "8.jpg"));
        return list;
    }
    
}
